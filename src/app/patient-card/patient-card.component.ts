import {Component, OnInit} from '@angular/core';
import {PatientCardService} from '../shared/patient-card.service';
import {PatientCard} from '../shared/patient-card.model';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-patient-card',
  templateUrl: './patient-card.component.html',
  styles: []
})
export class PatientCardComponent implements OnInit {

  constructor(public service: PatientCardService, private toastr: ToastrService) {
  }

  ngOnInit(): void {
    this.service.refreshList();
  }

  // tslint:disable-next-line:typedef
  populateForm(selectedRecord: PatientCard) {
    this.service.formData = Object.assign({}, selectedRecord);
  }

  // tslint:disable-next-line:typedef
  onDelete(id: string) {
    if (confirm('Are you sure to delete this record?')) {
      this.service.deletePatientCardDetail(id).subscribe(
        res => {
          this.service.refreshList();
          this.toastr.error('Deleted successfully', 'Patient Card Register');
        },
        err => {
          console.log(err);
        }
      );
    }
  }

}
