import {Component, OnInit} from '@angular/core';
import {PatientCardService} from '../../shared/patient-card.service';
import {NgForm} from '@angular/forms';
import {PatientCard} from '../../shared/patient-card.model';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-patient-card-form',
  templateUrl: './patient-card-form.component.html',
  styles: []
})
export class PatientCardFormComponent implements OnInit {

  constructor(public service: PatientCardService, private toastr: ToastrService) {
  }

  ngOnInit(): void {
  }

  // tslint:disable-next-line:typedef
  onSubmit(form: NgForm) {
    if (this.service.formData.id == null) {
      this.insertRecord(form);
    } else {
      this.updateRecord(form);
    }
  }

  // tslint:disable-next-line:typedef
  insertRecord(form: NgForm) {
    this.service.postPatientCardDetail().subscribe(
      res => {
        this.resetForm(form);
        this.service.refreshList();
      },
      err => {
        this.toastr.success('Submitted successfully', 'Pacient-card');
        console.log(err);
      }
    );
  }

  // tslint:disable-next-line:typedef
  resetForm(form: NgForm) {
    form.form.reset();
    this.service.formData = new PatientCard();
  }

  // tslint:disable-next-line:typedef
  updateRecord(form: NgForm) {
    this.service.putPatientCardDetail().subscribe(
      res => {
        this.resetForm(form);
        this.service.refreshList();
      },
      err => {
        this.toastr.info('Updated successfully', 'Patient-card');
        console.log(err);
      }
    );
  }


}
