import {Injectable} from '@angular/core';
import {PatientCard} from './patient-card.model';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PatientCardService {

  constructor(private http: HttpClient) {
  }

  formData: PatientCard = new PatientCard();
  readonly baseUrl = 'http://localhost:59894/api/PatientCard';
  list: PatientCard[] | undefined;

  // tslint:disable-next-line:typedef
  postPatientCardDetail() {
    return this.http.post(this.baseUrl, this.formData);
  }

  // tslint:disable-next-line:typedef
  putPatientCardDetail() {
    return this.http.put(`${this.baseUrl}/${this.formData.id}`, this.formData);
  }

  // tslint:disable-next-line:typedef
  deletePatientCardDetail(id: string) {
    return this.http.delete(`${this.baseUrl}/${id}`);
  }

  // tslint:disable-next-line:typedef
  refreshList() {
    this.http.get(this.baseUrl).toPromise().then(res => this.list = res as PatientCard[]);
  }
}
