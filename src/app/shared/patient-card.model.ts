export class PatientCard {
  id = '';
  iin = '';
  fullname = '';
  address = '';
  number = '';
  diagnosis = '';
  complaints = '';
  dateOfVisit = '';
  specialist = '';
}
